#!/usr/bin/python
__author__      = 'dan'

PNG_MASK        = ".png"

MODE_SIMPLE     = 1
MODE_IOS        = 2
MODE_ANDROID    = 3

HDPI_SCALE      = 0.75
HALF_SCALE      = 0.5
QUATER_SCALE    = 0.25
NO_SCALE        = 1

PREFIX_HD       = "-hd"
PREFIX_ipadhd   = "-ipadhd"
PREFIX_EMPTY    = ""



import logging
import os
import shutil
import sys
import time

# PIL import
import Image

logging.basicConfig(level=logging.INFO)


def scale_image(original_file_path, new_name, scale_factor = HALF_SCALE):
    if(scale_factor == NO_SCALE):
        if os.path.exists(new_name):
            os.rename(new_name, "%s%s.bak" % (new_name, str(time.clock())))
        shutil.copyfile(original_file_path, new_name)

    else:
        im         = Image.open(original_file_path)
        new_width  = int (scale_factor * im.size[0])
        new_height = int (scale_factor * im.size[1])
        im         = im.resize((new_width, new_height), Image.ANTIALIAS)
        im         = im.save(new_name)


def prefix_free_name(name):
    for suffix in [ PREFIX_HD, PREFIX_ipadhd ]:
        if name is not None and name.endswith(suffix):
            return name[:-len(suffix)]

    return name

def add_suffix(name, suffix=PREFIX_EMPTY):
    return "%s%s.png" % (name, suffix)

def create_path(path, base_name, suffix=PREFIX_EMPTY):
    return os.path.join(path, add_suffix(base_name, suffix))

def create_folder(path, folder_name):
    newpath = os.path.join(path, folder_name)
    if not os.path.exists(newpath): os.makedirs(newpath)
    return newpath


def scale_images_in_path(path, scale_factor = HALF_SCALE, mode = MODE_ANDROID, is_recursive = 0) :
    file_listing = os.listdir(path)
    file_listing.sort()

    for image_path in file_listing:

        full_path = os.path.join(path, image_path)

        if (os.path.isdir(full_path) and is_recursive):
            scale_images_in_path(full_path, scale_factor)

        else:
            file_name, file_extension = os.path.splitext(image_path)

            if PNG_MASK.upper() == file_extension.upper():
            
                if mode == MODE_SIMPLE:
                    scale_image(full_path, full_path)

                elif mode == MODE_IOS:
                    base_name = prefix_free_name(file_name)

                    create_path_by_suffix = lambda s: create_path(path, base_name, s)

                    #rename base picture for ipadhd
                    scale_image(full_path, create_path_by_suffix(PREFIX_ipadhd), NO_SCALE)
                    #save hd picrute
                    scale_image(full_path, create_path_by_suffix(PREFIX_HD)    , HALF_SCALE)
                    #save picture for non hd iphone
                    scale_image(full_path, create_path_by_suffix(PREFIX_EMPTY) , QUATER_SCALE)

                elif mode == MODE_ANDROID:

                    base_name = prefix_free_name(file_name)
                    create_path_by_suffix = lambda s: create_path(create_folder(path, s), base_name)

                    scale_image(full_path, create_path_by_suffix("drawable-xhdpi"), NO_SCALE)
                    scale_image(full_path, create_path_by_suffix("drawable-hdpi") , HDPI_SCALE)
                    scale_image(full_path, create_path_by_suffix("drawable")      , HALF_SCALE)
                    scale_image(full_path, create_path_by_suffix("drawable-ldpi") , QUATER_SCALE)



def main():
    if len(sys.argv) < 2 :
        logging.error ("you must provide an images dir path for script")
        sys.exit()

    logging.info("Script paramters are: %s" % str(sys.argv))
    script_path   = os.path.normpath(sys.argv[1])

    scale_images_in_path(script_path)

if __name__ == "__main__" :
    main()
